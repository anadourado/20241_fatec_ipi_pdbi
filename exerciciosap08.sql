DO $$
DECLARE 
	valor INTEGER; 
	valor_real NUMERIC(5, 1);
	Fahrenheit NUMERIC(5, 1);
	valor_a INTEGER;
	valor_b INTEGER;
	valor_c INTEGER;
	delta NUMERIC(5, 1);
	n1 INTEGER;
	raiz_3 NUMERIC(5, 1);
	raiz_2 NUMERIC(5, 1);
	comprimento NUMERIC(5, 1);
	largura NUMERIC(5, 1);
	preco_total NUMERIC(5, 1);
	m2 NUMERIC(5, 1);
	ano_nascimento INTEGER;
	ano_atual INTEGER;
	idade INTEGER;
	
BEGIN
	-- 1.1 Faça um programa que gere um valor inteiro e o exiba.
	valor := random() * (100 - 1) + 1; 
	RAISE NOTICE 'Valor inteiro gerado: %', valor;
	
	-- 1.2 Faça um programa que gere um valor real e o exiba.
	valor := random() * (10 - 1) + 1;
	RAISE NOTICE 'Valor real gerado: %', valor;
	
	-- 1.3 Faça um programa que gere um valor real no intervalo [20, 30] que representa uma temperatura em graus Celsius. Faça a conversão para Fahrenheit e exiba.
	valor_real := random() * (30 - 20) + 20;
	Fahrenheit := valor_real * 1.8 + 32;
	RAISE NOTICE 'Temperatura gerada em Celsius: % ', valor_real;
	RAISE NOTICE 'Temperatura em Fahrenheit: % ', Fahrenheit;
	
	--1.4 Faça um programa que gere três valores reais a, b, e c e mostre o valor de delta: aquele que calculamos para chegar às potenciais raízes de uma equação do segundo grau.
	valor_a := random() * (10 - 1) + 1;
	valor_b := random() * (10 - 1) + 1;
	valor_c := random() * (10 - 1) + 1;
	delta :=  valor_b^2 - 4 * valor_a * valor_c;
	RAISE NOTICE 'Valor de a: %', valor_a;
	RAISE NOTICE 'Valor de b: %', valor_b;
	RAISE NOTICE 'Valor de c: %', valor_c;
	RAISE NOTICE 'Valor do gerado do Delta: %', delta;
	
	--1.5 Faça um programa que gere um número inteiro e mostre a raiz cúbica de seu antecessor e a raiz quadrada de seu sucessor.
	n1 := random() * (100 - 1) + 1;
	raiz_3 := ||/(n1 - 1);
	raiz_2 := |/(n1 + 1);
	RAISE NOTICE 'Número inteiro gerado: %', n1;
	RAISE NOTICE 'A raiz cúbica do seu antecessor: %', raiz_3;
	RAISE NOTICE 'A raiz quadrada do seu sucessor: %', raiz_2;
	
	--1.6 Faça um programa que gere medidas reais de um terreno retangular. Gere também um valor real no intervalo [60, 70] que representa o preço por metro quadrado. O programa deve exibir o valor total do terreno.
	comprimento := random() * (10 - 1) + 1;
	largura := random() * (10 - 1) + 1;
	m2 := random() * (70 - 60) + 60;
	preco_total := (comprimento * largura) * m2;
	RAISE NOTICE 'O preco total do terreno é: %', preco_total;
	
	--1.7 Escreva um programa que gere um inteiro que representa o ano de nascimento de uma pessoa no intervalo [1980, 2000] e gere um inteiro que representa o ano atual no intervalo [2010, 2020]. O programa deve exibir a idade da pessoa em anos. Desconsidere detalhes envolvendo dias, meses, anos bissextos etc
	ano_nascimento := random() * (2000 - 1980) + 1980;
	ano_atual := random() * (2020 - 2010) + 2010;
	idade := ano_atual - ano_nascimento;
	RAISE NOTICE 'A idade da pessoa em anos é: %', idade;
END;
$$